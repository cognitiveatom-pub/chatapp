import { Routes } from '@angular/router';
import { ChatPanelComponent } from './chat-panel/chat-panel.component';
import { HomePageComponent } from './home-page/home-page.component';

export const routes: Routes = [
    { path: '', component:  HomePageComponent},
];
