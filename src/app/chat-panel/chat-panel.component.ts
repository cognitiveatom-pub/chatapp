import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-chat-panel',
  standalone: true,
  imports: [CommonModule, RouterOutlet],
  templateUrl: './chat-panel.component.html',
  styleUrl: './chat-panel.component.css'
})
export class ChatPanelComponent implements OnInit {
  imageUrls: string[] = [
    './assets/images/backgrounds/bg1.jpeg',
    './assets/images/backgrounds/bg2.jpeg',
    './assets/images/backgrounds/bg3.jpeg'
  ];
  currentImageUrl: string = this.imageUrls[0];
  isChatOpen: boolean = false;
  messages: any[] = [];
  newMessage: string = '';

  constructor() {}

  ngOnInit(): void {
    this.startImageChangeInterval();
  }

  startImageChangeInterval(): void {
    setInterval(() => {
      this.changeBackgroundImage();
    }, 5000); // 5 seconds interval
  }

  changeBackgroundImage(): void {
    const currentIndex = this.imageUrls.indexOf(this.currentImageUrl);
    const nextIndex = (currentIndex + 1) % this.imageUrls.length;
    this.currentImageUrl = this.imageUrls[nextIndex];
  }

  openChat() {
    this.isChatOpen = true;
  }

  closeChat() {
    this.isChatOpen = false;
  }

  sendMessage(): void {
    // Add message to the list (replace with actual message sending logic)
    this.messages.push({
      sender: 'me',
      content: this.newMessage,
      timestamp: new Date().toLocaleTimeString()
    });
    this.newMessage = '';
  }
}
